<p>Dear {{ $title }} {{ $name }} {{ $surname }}</p>

<p>
    We hereby provide approval that all your subimited data are correct.
</p>

<p>
    Please find attached Payment Request for the Outstanding Government Fees. Payment should be made before processing step 10. Please note that the
    system will not allow you to commence beyond processing point 10 if you have not completed payment.
</p>

<p>
    You may logon to the Newlands - Dominica Direct Online system and commence onto the next <b>step 8 <i>"Download Government Forms"</i></b>.
</p>

{!! config('hpsamailer.signature') !!}
