<p>Dear {{ $title }} {{ $name }} {{ $surname }}</p>

<p>
    Thank you for making payment.
</p>

<p>
    Please find attached Receipt of payment.
</p>

<p>
    You may logon to the Newlands - Dominica Direct Online system and commence onto the next <b>step 11 <i>"Courier Government Forms"</i></b>.
</p>

{!! config('hpsamailer.signature') !!}
