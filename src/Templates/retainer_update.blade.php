<p>Dear {{ $title }} {{ $name }} {{ $surname }}</p>

<p>
    Thank you for making payment.
</p>

<p>
    Please find attached Receipt of payment along with the Applicaiton Guidlines.
</p>

<p>
    You may logon to the Newlands - Dominica Direct Online system and commence onto the next <b>step 5 <i>"Data Input"</i></b>.
</p>

{!! config('hpsamailer.signature') !!}
