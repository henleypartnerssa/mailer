<p>Dear {{ $name }} {{ $lastname }}</p>

<p>
    Please Verify your account email by clicking the following <a href="{{ $url }}" >Click Here to Verify email</a>
</p>

{!! config('hpsamailer.signature') !!}
