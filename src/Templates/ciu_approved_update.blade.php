<p>Dear {{ $title }} {{ $name }} {{ $surname }}</p>

<p>
    We are happy to confirm that you have CIU Approval.
</p>

<p>
    Please see attached confirmation letter of approval.
</p>

<p>
    You may logon to the Newlands - Dominica Direct Online system and commence onto the next <b>step 14 <i>"Investment Overview"</i></b>.
</p>

{!! config('hpsamailer.signature') !!}
