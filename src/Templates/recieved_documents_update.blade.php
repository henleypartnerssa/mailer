<p>Dear {{ $title }} {{ $name }} {{ $surname }}</p>

<p>
    Hereby we would like to confirm that we have recieved your package via Courier.
</p>

<p>
    Your package will now be reviewed to validate if all information are correct.
</p>

<p>
    You may logon to the Newlands - Dominica Direct Online system and commence onto the next <b>step 12 <i>"Package Review"</i></b>.
</p>

{!! config('hpsamailer.signature') !!}
