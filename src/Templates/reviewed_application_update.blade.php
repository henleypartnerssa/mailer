<p>Dear {{ $title }} {{ $name }} {{ $surname }}</p>

<p>
    Hereby we would like to confirm that all information provided are correct.
</p>

<p>
    Your package will now be reviewed to validate if all information are correct. Process of submitting your package to goverment firms will now commence.
</p>

<p>
    You may logon to the Newlands - Dominica Direct Online system and commence onto the next <b>step 13 <i>"Package Submission"</i></b>.
</p>

{!! config('hpsamailer.signature') !!}
