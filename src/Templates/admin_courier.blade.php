<p>Dear Admin Team</p>

<p>Note that Principal Applicant <b>{{$first_name}} {{$last_name}}</b> has couried his/her package via the following details:</p>

<p>
<b>Courier Type: </b> {{$courier}} <br>
<b>Tracking No: </b> {{$track_no}} <br>
<b>Applcation: </b> {{$appName}} (Internal use only)
</p>

<p>Please action.</p>

{!! config('hpsamailer.system_sig') !!}
