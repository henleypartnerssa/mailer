<p>Dear {{ $title }} {{ $name }} {{ $surname }}</p>

<p>
    <b><i>Congratulations!!!</i></b> You have successfully passed the World Due Diligence Check.
</p>

<p>
    Herewith, please sign and courier back to address listed below attached documents.
</p>

<p>
    133 Building XXX<br>
    Lodnon Avenue<br>
    London
</p>

<p>
    Please logon to Newlands Dominica Direct Online Application system and proceed onto the next step <b>"Request For Payment"</b>.
</p>

{!! config('hpsamailer.signature') !!}
