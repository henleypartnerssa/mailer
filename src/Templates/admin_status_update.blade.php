<p>Dear Admin Team</p>

<p>Note that Principal Applicant <b>{{$first_name}} {{$last_name}}</b> has reached the <b>{{$state}}</b> phase of his/her application namely: <b>{{$appName}}</b></p>

<p>Please action.</p>

{!! config('hpsamailer.system_sig') !!}
