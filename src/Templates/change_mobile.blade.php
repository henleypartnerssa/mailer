<p>Dear {{ $name }} {{ $lastname }}</p>

<p>
    Please click on the following link to verify your mobile number change: <a href="{{ $url }}" >Click Here to Verify mobile number change</a>
</p>

{!! config('hpsamailer.signature') !!}
