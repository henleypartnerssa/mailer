<?php

namespace hpsadev\Mailer;

use Illuminate\Support\ServiceProvider;

class MailerServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadViewsFrom(__DIR__.'/Templates', 'hpsamailer');

        $this->publishes([
            __DIR__.'/hpsamailer.php'  => config_path('hpsamailer.php'),
            __DIR__.'/Templates'        => base_path("resources/views/vendor/hpsamailer"),
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
