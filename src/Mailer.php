<?php

namespace hpsadev\Mailer;

use Mail;
use Crypt;
use Storage;

use AppHelpers;

/**
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.3
 * Class containing static mailer functions. Standard of
 * settings array listed below:
 * 		user           = object
 * 		data           = []
 * 		view           = optional string
 * 		profile        = optional object
 * 		attachements   = [optional]
 *   	url            = optional string
 *   	raw            = optional string
 *   	username       = optional string
 */
class Mailer
{

    /**
     * User Verification Email.
     * @since  0.0.1 Function Init
     * @param  array  $options mail options
     * @return bool          bool value mail sent or not.
     */
    public static function verificationMail($options = array()) {
        $opt            = (object) $options;
        $data           = $opt->data;
        $data['url']    = Mailer::buildUrl("users/verify", $opt->user->user_id);
        $mailList       = $opt->user->username;
        $sent           = Mail::send('hpsamailer::verification', $data, function($msg) use ($mailList) {
            $msg->to($mailList)->subject(env("COMPANY").' - Account Verification');
        });
        return $sent;
    }

    /**
     * Email sent when user change mobile number.
     * @since  0.0.1 Function Init
     * @param  array  $options mail options
     * @return bool          bool value mail sent or not.
     */
    public static function mobileEmail($options = array()) {
        $opt            = (object) $options;
        $data           = $opt->data;
        $data['url']    = Mailer::buildUrl("users/mobile/verify", $opt->user->username);
        $mailList       = $opt->user->username;
        $sent           = Mail::send('hpsamailer::change_mobile', $data, function($msg) use ($mailList) {
            $msg->to($mailList)->subject(env("COMPANY").' - Change Mobile Number Verification');
        });
        return $sent;
    }

    /**
     * User email sent to admin when User have reached
     * process showstopper.
     * @since  0.0.1 Function Init
     * @param  array  $options mail options
     * @return bool          bool value mail sent or not.
     */
    public static function adminStatusUpdate($options = array()) {
        $opt            = (object) $options;
        $data           = $opt->data;
        $mailList       = config('hpsamailer.admin_mail_list');
        $sent           = Mail::send('hpsamailer::admin_status_update', $data, function($msg) use ($mailList, $data) {
            $msg->to($mailList)->subject(env("COMPANY").' - Application:'.$data['appName'].' Status Update');
        });
        return $sent;
    }

    /**
     * User mail sent to Admin when user have submitted
     * courier data.
     * @param  array  $options mail options
     * @return bool          bool value mail sent or not.
     */
    public static function adminCourier($options = array()) {
        $opt            = (object) $options;
        $data           = $opt->data;
        $mailList       = config('hpsamailer.admin_mail_list');
        $sent           = Mail::send('hpsamailer::admin_courier', $data, function($msg) use ($mailList, $data) {
            $msg->to($mailList)->subject(env("COMPANY").' - Application:'.$data['appName'].' Status Update');
        });
        return $sent;
    }

    /**
     * Admin email sent to user when admin has provided approval.
     * @since  0.0.1    Function Init
     * @since  0.0.3    Fixed Email Subject Spelling
     *         			Added - Return 0 if attachement not found.
     * @param  array    $options mail options
     * @return bool     bool value mail sent or not.
     */
    public static function stepApproval($options = array()) {
        $opt            = (object) $options;
        $data           = $opt->data;
        $mailList       = $opt->user->username;
        $attachements   = (!empty($opt->attachements)) ? $opt->attachements : NULL;

        if (in_array(NULL,$attachements)) {
            return 0;
        } else {
            $sent = Mail::send('hpsamailer::'.$opt->view, $data, function($msg) use ($mailList, $attachements) {
                $msg->to($mailList)->subject(env("COMPANY").' - Citizenship by Investment Application - Status Update');
                if (count($attachements) > 0) {
                    foreach ($attachements as $attachement) {
                        $msg->attach($attachement);
                    }
                }
            }, true);
        }
        return $sent;
    }

    /**
     * Customisable email sent from Admin to User.
     *
     * @since  0.0.1    Function Init
     * @since  0.0.3    Fixed Email Subject Spelling
     *         			Added - Return 0 if attachement not found.
     *
     * @param  array    $options mail options
     * @return bool     bool value mail sent or not.
     */
    public static function customEmail($options = array()) {
        $data           = $options['raw'];
        $mailList       = $options['username'];
        $htmlFile       = Mailer::tmpHtmlFile($data);
        $attachements   = (!empty($options['attachements'])) ? $options['attachements'] : NULL;
        if (is_array(NULL,$attachements)) {
            unlink($htmlFile['file']);
            return 0;
        } else {
            $sent = Mail::send('hpsamailer::'.$htmlFile['view'], [], function($msg) use ($mailList,$attachements) {
                $msg->to($mailList)->subject(env("COMPANY").' - Citizenship by Investment Application - Status Update');
                if (count($attachements) > 0) {
                    foreach ($attachements as $attachement) {
                        $msg->attach($attachement);
                    }
                }
            }, true);
            unlink($htmlFile['file']);
        }

        return $sent;
    }

    /**
     * Build and Get temporary HTML mail file.
     * @since  0.0.1 Function Init
     * @param  string $html html to be written to file.
     * @return array       view file and full file path
     */
    public static function tmpHtmlFile($html) {
        // NOTE: Perhaps user In-Memory File during future Version!!!....
        $rndm = AppHelpers::randomStr();
        $file = $rndm.'.blade.php';
        $HTML = Mailer::enrichData($html);
        $createFile = storage_path('app/mail/').$file;
        Storage::put('mail/'.$file, $HTML);
        $newFile    = __DIR__.'/Templates'.'/'.$file;
        rename($createFile, $newFile);
        return [
            'view' => $rndm,
            'file' => $newFile
        ];
    }

    /**
     * Enrich raw string with html tags.
     * @since  0.0.1 Function Init
     * @param  string $rawData raw message data.
     * @return string          enriched string with html tags
     */
    public static function enrichData($rawData) {
        $raw        = str_replace("\\\\n", "~", $rawData);
        $dec        = str_replace('~~', '\n', $raw);
        $final      = str_replace('~', '<br>', $dec);
        $splitData  = explode('\n', $final);
        $contents   = '';
        foreach ($splitData as $d) {
            $contents = $contents.'<p>'.$d.'</p>';
        }
        return $contents;
    }

    /**
     * Internal help func for building encrypted URI.
     * @since  0.0.1 Function Init
     * @param  string $url URL
     * @param  string $key value to encrypt
     * @return string      compiled URI.
     */
    public static function buildUrl($url, $key) {
        $url        = $url;
        $secret     = Crypt::encrypt($key);
        $uri        = url($url, $parameters = array($secret), $secure = false);
        return $uri;
    }

    /**
     * Internal helper function lookup email view name.
     *
     * @since  0.0.2 Function Init
     * @since  0.0.3 Added commenting to this function.
     *
     * @param  [type] $approval [description]
     * @return [type]           [description]
     */
    public static function lkpView($approval) {
        switch ($approval) {
            case 'dd':
                return 'dd_check_update';
                break;
            case 'retainer':
                return 'retainer_update';
                break;
            case 'dr':
                return 'data_review_update';
                break;
            case 'balance':
                return 'balance_paid_update';
                break;
            case 'gov_docs':
                return 'recieved_documents_update';
                break;
            case 'review':
                return 'reviewed_application_update';
                break;
            case 'submitted':
                return 'gov_submit_update';
                break;
            case 'ciu':
                return 'ciu_approved_update';
                break;
            case 'investment':
                return 'investment_complete_update';
                break;
            case 'oath':
                return 'oath_update';
                break;
            case 'passport':
                return 'passport_update';
                break;
            case 'dominica':
                return 'dominica_update';
                break;
        }
    }

    /**
     * Lookup the email attachement per specifie approval.
     * @since  0.0.1 Function Init
     * @param  string       $approval   apporval type
     * @param  FileManager  $fm         Instace of FileManager
     * @param  profile      $profile    instance of Profile
     * @return array                    array of attachements
     */
    public static function lkpAttachemnts($approval, $fm, $profile = null) {
        $attachements = [];
        switch ($approval) {
            case 'dd':
                $attachements = [
                    $fm->getFileByName("Client Agreement A", storage_path('app/admin/')),
                    $fm->getFileByName("Client Agreement B", storage_path('app/admin/')),
                    $fm->getFileByName("PAO", storage_path('app/admin/')),
                    $fm->getFileByName("Payment-Request-Service-Fees", storage_path('app/users/'.$profile->user_id.'/payments/'))
                ];
                break;
            case 'retainer':
                $attachements = [
                    $fm->getFileByName("Application Guidelines", storage_path('app/admin/')),
                    $fm->getFileByName("Service-Fees-Receipt", storage_path('app/users/'.$profile->user_id.'/receipts/'))
                ];
                break;
            case 'dr':
                $attachements = [
                    $fm->getFileByName("Payment-Request-Government-Fees", storage_path('app/users/'.$profile->user_id.'/payments/'))
                ];
                break;
            case 'balance':
                $attachements = [
                    $fm->getFileByName("Government-Fees-Receipt", storage_path('app/users/'.$profile->user_id.'/receipts/'))
                ];
                break;
            case 'ciu':
                $attachements = [
                    $fm->getFileByName("Government Approval Letter", storage_path('app/users/'.$profile->user_id.'/approvals/'))
                ];
                break;
        }
        return $attachements;
    }

}
