# Mailer

This is a Mailer Package that contains all the logic and functionality require by HP Online Sales Applications.

## Install

Via Composer

``` bash
$ composer require hpsadev/mailer
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## License

The HP Private Commercial License.
